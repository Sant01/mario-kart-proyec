
/*
 * File:   pic.c
 * Author: SANTIAGO TESILLO, JORGE RODRIGUEZ, MANUEL CASTILLA
 *
 * Created on 25 de octubre de 2022, 08:02 PM
 */

#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 


#define _XTAL_FREQ 20000000



int main(void){
     PORTB = 0xFF;
     TRISB = 0x00;
     PORTD = 0x00;
     TRISD = 0x00;
     ANSELH = 0x00;
     
    
     while(1){
         if(RB0==0){
             __delay_ms(100);
             if(RB0==0)
             {
             PORTD = 0x05;
             }     
         
         }
         
         if(RB1==0){
             __delay_ms(100);
             if(RB1==0)
             {
             PORTD = 0x0A;
             }     
         
         }
         
         if(RB2==0){
             __delay_ms(100);
             if(RB2==0)
             {
             PORTD = 0x08;
             }     
         
         }
         
         if(RB3==0){
             __delay_ms(100);
             if(RB3==0)
             {
            PORTD = 0x02;
             }     
         
         }
         
         if(RB4==0){
             __delay_ms(100);
             if(RB4==0)
             {
             PORTD = 0x00;
             }     
         
         }
         
     }
}
